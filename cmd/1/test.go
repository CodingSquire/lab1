package main

import (
	"encoding/json"
	"fmt"
)

type myStruct struct {
	A int     `json:"a222"`
	B float64 `json:"bac"`
	C string  `json:"cax"`
	D int     `json:"dec"`
}

func main() {

	m1 := myStruct{
		A: 0,
		B: 3.14,
		C: "some text",
		D: 0,
	} //{"A":0,"B":3.14,"C":"some text"}

	data, err := json.Marshal(m1)
	fmt.Println(data, err)

	m2 := myStruct{}
	err = json.Unmarshal(data, &m2)

	fmt.Println(err)
}
