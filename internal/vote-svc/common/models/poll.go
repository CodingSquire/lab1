package models

type KeyItem int

type Poll struct {
	Items map[KeyItem]Item `json:"items"`
}

type Item struct {
	Name       string `json:"name"`
	NumberVote int    `json:"number_vote"`
}

func (i *Item) Vote() {
	i.NumberVote++
}
