package application

import (
	"fmt"
	"study/lection_mai_2022/lab1/internal/vote-svc/common/models/services"

	"study/lection_mai_2022/lab1/internal/vote-svc/common/models"
)

type VoteSvc struct {
	polls map[models.KeyItem]*models.Poll
}

var _ services.AppPoll = (*VoteSvc)(nil)

func NewVoteSvc() *VoteSvc {
	return &VoteSvc{
		polls: make(map[models.KeyItem]*models.Poll),
	}
}

//создать голосование c вариантами ответов
func (v *VoteSvc) CreatePoll(pollID models.KeyItem, items []models.Item) error {
	poll := &models.Poll{
		Items: make(map[models.KeyItem]models.Item),
	}
	for i, item := range items {
		poll.Items[models.KeyItem(i)] = item
	}
	if _, ok := v.polls[pollID]; ok {
		return fmt.Errorf("Уже имеется poll с polle_id '%v'", pollID)
	}
	v.polls[pollID] = poll

	return nil
}

// проголосовать за конкретный вариант: <poll_id, choice_id>

func (v *VoteSvc) VoteForItem(pollID models.KeyItem, ItemID models.KeyItem) error {
	poll, ok := v.polls[pollID]
	if !ok {
		return fmt.Errorf("Голосования с poll_id '%v' не существует ", pollID)
	}
	item, ok := poll.Items[ItemID]
	if !ok {
		return fmt.Errorf("Покемона с item_id '%v' не существует в голосовании с pole_id '%v' ", ItemID, pollID)
	}

	item.Vote()

	poll.Items[ItemID] = item

	return nil
}

// Получить результат по конкретному голосованию: <poll_id> //(вывести топ 5 по голосам)
func (v *VoteSvc) GetResult(pollID models.KeyItem) (leaderBoard []models.Item, err error) {
	poll, ok := v.polls[pollID]
	if !ok {
		err = fmt.Errorf("Голосования с poll_id '%v' не существует ", pollID)
		return
	}
	leaderBoard = make([]models.Item, 0, len(poll.Items))
	for _, item := range poll.Items {
		leaderBoard = append(leaderBoard, item)
	}
	return
}
