package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"study/lection_mai_2022/lab1/internal/vote-svc/common/models"
	"study/lection_mai_2022/lab1/internal/vote-svc/common/models/services"
	"study/old/go/src/fmt"
)

type VoteController struct {
	appVote services.AppPoll
}

func NewVoteController(appVote services.AppPoll) *VoteController {
	return &VoteController{
		appVote: appVote,
	}
}

type CreatePollReq struct {
	PollID models.KeyItem `json:"poll_id"`
	Items  []models.Item  `json:"items"`
}

func (v *VoteController) CreatePoll(writer http.ResponseWriter, request *http.Request) { //(pollID models.KeyItem, items []models.Item) error{
	body := request.Body
	data, err := ioutil.ReadAll(body)
	if err != nil {
		err = fmt.Errorf("error with creating poll:", err)
		http.Error(writer, err.Error(), http.StatusBadGateway)
	}
	var result CreatePollReq
	err = json.Unmarshal(data, &result)
	if err != nil {
		err = fmt.Errorf("error with creating poll:", err)
		http.Error(writer, err.Error(), http.StatusBadGateway)
	}
	pollID := result["poll_id"].(models.KeyItem)
	items := result["items"].([]models.Item)

	err = v.appVote.CreatePoll(pollID, items)

	if err != nil {
		err = fmt.Errorf("error with creating poll:", err)
		http.Error(writer, err.Error(), http.StatusBadGateway)
	}
}

func (v *VoteController) VoteForItem(writer http.ResponseWriter, request *http.Request) { //(pollID models.KeyItem, ItemID models.KeyItem) error{

}
func (v *VoteController) GetResult(writer http.ResponseWriter, request *http.Request) { //(pollID models.KeyItem) (leaderBoard []models.Item, err error){

}
